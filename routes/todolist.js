const express = require('express');
const router = express.Router();
const TodolistController = require('../controller/todolistController')

router.get('/todolist', TodolistController.findAll)
router.get('/todolist/:id', TodolistController.findOne)
router.post('/todolist', TodolistController.create)
router.delete('/todolist/:id', TodolistController.destroy)

module.exports = router;