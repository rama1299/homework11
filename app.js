const express = require('express');
const app = express();
const port = 3001
const router = require('./routes')
const errorHandler = require('./middlewares/errorhandler')

app.use(express.json())
app.use(express.urlencoded({extended: false}))

app.use(router)
app.use(errorHandler)

app.listen(port, function(err){
    if (err) console.log('error in server setup')
    console.log('server listening on port', port)
})