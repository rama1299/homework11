'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('TodoLists', [
      {
        title: 'Berangkat Kerja',
        description: 'jam 08:30',
        status: 'active',
        createdAt: new Date(),
        updatedAt:  new Date()
      },
      {
        title: 'Makan siang',
        description: 'makan dengan 4sehat 5sempurna',
        status: 'active',
        createdAt: new Date(),
        updatedAt:  new Date()
      },
      {
        title: 'Deadline',
        description: 'homework rakamin sisa 1 hari lagi',
        status: 'active',
        createdAt: new Date(),
        updatedAt:  new Date()
      }
    ])
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('TodoLists', null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
