const {TodoList} = require('../models');

class TodolistController{

    static findAll = async (req, res, next) => {

        try{
            const data = await TodoList.findAll({
                where: {
                    status: "active"
                }
            });
            res.status(200).json(data);
        }catch(err){
            next(err)
        }
    }

    static findOne = async (req, res, next) =>{

        const {id} = req.params
        try{
            const data = await TodoList.findOne({
                where: {
                    id
                }
            })

            if(data){
                res.status(200).json(data);
            }else{
                throw {name: 'ErrorNotFound'}
            }
        }catch(err){
            next(err)
        }
    }

    static create = async (req, res, next) =>{

        const {title, description, status} = req.body
        try{
            const data = await TodoList.create({title, description, status})
            res.status(200).json({message: 'Todo list created'});
        }catch(err){
            next(err)
        }
    }

    static destroy = async (req, res, next) =>{
        const {id} = req.params
        try{
            const data = await TodoList.update({
                status: "inactive"
            }, {
                where: {id}
            })
        
            if(data [0] === 1){
                res.status(200).json({
                    message: 'Todo list deleted'
                })
            }else{
                throw {name: 'ErrorNotFound'}
            }
        }catch(err){
            next(err)
        }
    }

}

module.exports = TodolistController